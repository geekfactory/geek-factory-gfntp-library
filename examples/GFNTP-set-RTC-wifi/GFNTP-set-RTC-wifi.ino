/**
   GeekFactory - "INNOVATING TOGETHER"
   Distribucion de materiales para el desarrollo e innovacion tecnologica
   www.geekfactory.mx

   RTC chip example for the GFNTP library. This example shows how to synchronize
   an external RTC chip with the internet time.
*/
#include <WiFi101.h>
#include <WiFiUdp.h>
#include <GFNTP.h>
#include <GFRTC.h>

// Configure here your network access parameters
const char *ssid     = "GEEKFACTORY";
const char *password = "geekwifinet";

// UDP object for NTP
WiFiUDP ntpUDP;
// Instance of GFNTP client, we can set some parameters here
GFNTP timeClient(ntpUDP);
// Structure that holds human readable time information;
struct timelib_tm tinfo;

void setup() {
  // Prepare serial interface
  Serial.begin(115200);

  // Prepare RTC and I2C interface
  GFRTC.begin(true);
  while (!Serial);

  // Show dialog to serial monitor
  Serial.println(F("----------------------------------------------------"));
  Serial.println(F("             GFNTP LIBRARY TEST PROGRAM             "));
  Serial.println(F("             https://www.geekfactory.mx             "));
  Serial.println(F("----------------------------------------------------"));
  Serial.print(F("Connecting to WiFi"));

  // Connect to WiFi network
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // We´re connected at this point
  Serial.println(F("\r\nConnected"));

  // Start GFNTP library
  timeClient.begin();

  // Set callback
  timeClient.setCallback(rtc_set_callback);

  // We can configure GFNTP library parameters using the get / set API
  timeClient.setUpdateInterval(3600);
}

void loop() {
  // Call the proccess method on a regular basis
  timeClient.process();

  // Convert to human readable format
  GFRTC.read(tinfo);

  // Send to serial port
  Serial.print(tinfo.tm_hour);
  printDigits(tinfo.tm_min);
  printDigits(tinfo.tm_sec);
  Serial.print(" ");
  Serial.print(tinfo.tm_mday);
  Serial.print("/");
  Serial.print(tinfo.tm_mon);
  Serial.print("/");
  Serial.print(timelib_tm2y2k(tinfo.tm_year));
  Serial.println();

  // Wait 1 second
  delay(1000);
}

void printDigits(int digits)
{
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

/**
   This function is called by GFNTP library when a time update
   is received from NTP server.
*/
void rtc_set_callback(uint32_t t)
{
  // Set the RCT with the received timestamp
  GFRTC.set(t);
}
