/**
   GeekFactory - "INNOVATING TOGETHER"
   Distribucion de materiales para el desarrollo e innovacion tecnologica
   www.geekfactory.mx

   Basic example for the GFNTP library. This example shows how to get time as
   a Unix timestamp using WiFi Shield 101 or Arduino MKR 1000 WiFi or Adafruit
   compatible boards.
*/
#include <GFNTP.h>
#include <WiFi101.h>
#include <WiFiUdp.h>

// Configure here your network access parameters
const char *ssid     = "GEEKFACTORY";
const char *password = "geekwifinet";

// UDP object for NTP
WiFiUDP ntpUDP;
// Instance of GFNTP client
GFNTP timeClient(ntpUDP);

void setup() {
  // Prepare serial interface
  Serial.begin(115200);
  while (!Serial);

  // Show dialog to serial monitor
  Serial.println(F("----------------------------------------------------"));
  Serial.println(F("             GFNTP LIBRARY TEST PROGRAM             "));
  Serial.println(F("             https://www.geekfactory.mx             "));
  Serial.println(F("----------------------------------------------------"));
  Serial.print(F("Connecting to WiFi"));
  
  // Connect to WiFi network
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // We´re connected at this point
  Serial.println(F("\r\nConnected"));

  // Start GFNTP library
  timeClient.begin();
}

void loop() {
  // Call the proccess method on a regular basis
  timeClient.process();

  // Print the time every second, note that this is a Unix timestamp
  Serial.println(timeClient.getTime());
  delay(1000);
}
