# Geek Factory GFNTP Library for Arduino #

This library implements the Network time protocol for Arduino devices.

The NTP protocol allows to synchronize the device´s clock with the time of an NTP server on the internet or local area network.

The library is designed to work with Ethernet and WiFi arduino boards, for example the Arduino UNO / Mega with Ethernet shield or the Arduino MRK1000 which is WiFi enabled.


## Basic library usage ##

The following example is the basic implementation of an NTP client using this library. Source code is commented for easy understanding.

```cpp
#include <GFNTP.h>
#include <WiFi101.h>
#include <WiFiUdp.h>

// Configure here your network access parameters
const char *ssid     = "GEEKFACTORY";
const char *password = "geekwifinet";

// UDP object for NTP
WiFiUDP ntpUDP;
// Instance of GFNTP client
GFNTP timeClient(ntpUDP);

void setup() {
  // Prepare serial interface
  Serial.begin(115200);
  while (!Serial);

  // Show dialog to serial monitor
  Serial.println(F("----------------------------------------------------"));
  Serial.println(F("             GFNTP LIBRARY TEST PROGRAM             "));
  Serial.println(F("             https://www.geekfactory.mx             "));
  Serial.println(F("----------------------------------------------------"));
  Serial.print(F("Connecting to WiFi"));
  
  // Connect to WiFi network
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // We´re connected at this point
  Serial.println(F("\r\nConnected"));

  // Start GFNTP library
  timeClient.begin();
}

void loop() {
  // Call the proccess method on a regular basis
  timeClient.process();

  // Print the time every second, note that this is a Unix timestamp
  Serial.println(timeClient.getTime());
  delay(1000);
}
```

## Project objectives ##

* Create a reference implementation of the NTP protocol for Arduino.
* Minimize the resource usage of the implementation.
* Implementation that respects the resources and bandwith of NTP servers.

## Supported devices ##

The library has been tested on the following hardware:

* Arduino UNO + Ethernet Shield.
* Arduino MEGA + Ethernet Shield.
* Arduino MEGA + Adafruit WINC1500 WiFi Shield.
* Arduino MRK1000 WiFi.


## Contact me ##

* Feel free to write for any inquiry: ruben at geekfactory.mx 
* Check our website: https://www.geekfactory.mx

