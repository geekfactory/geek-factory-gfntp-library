/* Geek Factory GFNTP Library for Arduino
 * 
 * Copyright (C) 2018 Jesus Ruben Santa Anna Zamudio.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Author website: https://www.geekfactory.mx
 * Author e-mail: ruben at geekfactory dot mx
 */
#include "GFNTP.h"

/*-------------------------------------------------------------*
 *		Macros for debugging				*
 *-------------------------------------------------------------*/
#ifdef CONFIG_GFNTP_DEBUG
#define DEBUG_PRINT(...)	CONFIG_GFNTP_DEBUGSTREAM.print(__VA_ARGS__)
#define DEBUG_PRINTLN(...)	CONFIG_GFNTP_DEBUGSTREAM.println(__VA_ARGS__)
#else
#define DEBUG_PRINT(...)
#define DEBUG_PRINTLN(...)
#endif

GFNTP::GFNTP(UDP & udp, uint32_t timeOffset, uint32_t updateInterval) : _udp(udp), _timeZone(timeOffset), _updateInterval(updateInterval)
{
}

GFNTP::GFNTP(UDP& udp, const char* serverName, uint32_t timeOffset, uint32_t updateInterval) : _udp(udp), _serverName(serverName), _timeZone(timeOffset), _updateInterval(updateInterval)
{
}

void GFNTP::setServerName(const char * serverName)
{
	DEBUG_PRINTLN(F("\tGFNTP::setServerName()"));
	_serverName = serverName;
	DEBUG_PRINT(F("\tGFNTP::_serverName="));
	DEBUG_PRINTLN(_serverName);
}

void GFNTP::setTimeZone(int16_t timeZone)
{
	DEBUG_PRINTLN(F("\tGFNTP::setTimeZone()"));
	// basic check of time zone information
	if (timeZone > 14 || timeZone < -12) {
		DEBUG_PRINTLN(F("\tGFNTP:warning: invalid time zone, using 0 instead"));
		_timeZone = 0;
	} else {
		_timeZone = timeZone;
	}
	DEBUG_PRINT(F("\tGFNTP::_timeZone="));
	DEBUG_PRINTLN(_timeZone);
}

uint16_t GFNTP::getTimeZone()
{
	DEBUG_PRINTLN(F("\tGFNTP::getTimeZone()"));
	DEBUG_PRINT(F("\tGFNTP::_timeZone="));
	DEBUG_PRINTLN(_timeZone);
	return _timeZone;
}

void GFNTP::setUpdateInterval(uint32_t updateInterval)
{
	DEBUG_PRINTLN(F("\tGFNTP::setUpdateInterval()"));
	_updateInterval = updateInterval;
	DEBUG_PRINT(F("\tGFNTP::_updateInterval="));
	DEBUG_PRINTLN(_updateInterval);
}

uint32_t GFNTP::getUpdateInterval()
{
	DEBUG_PRINTLN(F("\tGFNTP::getUpdateInterval()"));
	DEBUG_PRINT(F("\tGFNTP::_updateInterval="));
	DEBUG_PRINTLN(_updateInterval);

	return _updateInterval;
}

void GFNTP::setCallback(gfntpcb_t callback)
{
	DEBUG_PRINTLN(F("\tGFNTP::setCallback()"));
	_onTimeReceived = callback;

}

uint32_t GFNTP::getTime()
{
	return _currentTime;
}

void GFNTP::forceUpdate()
{
	DEBUG_PRINTLN(F("\tGFNTP::forceUpdate()"));
	// set flag to force NTP update
	_forceFlag = true;
}

void GFNTP::begin()
{
	DEBUG_PRINTLN(F("\tGFNTP::begin()"));

	// Print object state
	DEBUG_PRINT(F("\tGFNTP::_serverName="));
	DEBUG_PRINTLN(_serverName);
	DEBUG_PRINT(F("\tGFNTP::_timeZone="));
	DEBUG_PRINTLN(_timeZone);
	DEBUG_PRINT(F("\tGFNTP::_updateInterval="));
	DEBUG_PRINTLN(_updateInterval);
	DEBUG_PRINT(F("\tGFNTP::_errorUpdateInterval="));
	DEBUG_PRINTLN(_errorUpdateInterval);

	// Start couting time now
	_timeHelper = millis();

	// Update time on boot
	forceUpdate();
}

void GFNTP::process(bool netready)
{

	// update local unix time if needed
	while (millis() - _timeHelper >= 1000) {
		// Increment timestamp
		_currentTime++;
		_timeHelper += 1000;
	}

	// check if automatic update from server is enabled and if it�s time to update	
	bool nupdate = (_updateInterval != 0) && (millis() - _lastUpdate >= (_updateInterval * 1000));
	// check if error ocurred and need update sooner because of it
	bool eupdate = (_errorUpdateInterval != 0) && (millis() - _lastUpdate >= (_errorUpdateInterval * 1000));

	// NTP service state machine implementation
	switch (_gfntpstate) {
	case E_NTP_IDLE:

		if (_forceFlag || nupdate || eupdate) {
			if (netready) {
				if (_forceFlag) {
					DEBUG_PRINTLN(F("\tGFNTP:manual NTP update"));
				} else {
					DEBUG_PRINTLN(F("\tGFNTP:scheduled NTP update"));
				}
				if (_udp.begin(GFNTP_LOCAL_PORT)) {
					DEBUG_PRINTLN(F("\tGFNTP:begin udp ok"));
					_gfntpstate = E_NTP_SEND_REQUEST;
				} else {
					_errorUpdateInterval = GFNTP_ERROR_UPDATEINTERVAL;
					DEBUG_PRINT(F("\tGFNTP:begin udp fail, retry in "));
					DEBUG_PRINT(_errorUpdateInterval);
					DEBUG_PRINTLN(F(" seconds"));

				}
			} else {
				// normal update is required, but updates are disabled
				if (nupdate || _forceFlag) {
					DEBUG_PRINTLN(F("\tGFNTP:setting shorter update period"));
					_errorUpdateInterval = GFNTP_ERROR_UPDATEINTERVAL;
				}
				DEBUG_PRINT(F("\tGFNTP:update disabled, retry in "));
				DEBUG_PRINT(_errorUpdateInterval);
				DEBUG_PRINTLN(F(" seconds"));
			}
			// update some class members
			_retries = 0;
			_lastUpdate = millis();
			_forceFlag = false;
		}
		break;

	case E_NTP_SEND_REQUEST:
		DEBUG_PRINT(F("\tGFNTP:attempt "));
		DEBUG_PRINTLN(_retries);
		// clear incoming packets
		while (_udp.parsePacket() > 0);
		// send NTP request
		sendNTPRequest();
		// increase retries counter
		_retries++;
		// wait for response packet
		_timeout = millis();
		// advance to next state
		_gfntpstate = E_NTP_WAIT_RESPONSE;
		break;

	case E_NTP_WAIT_RESPONSE:
		// Check if response has been received
		if (_udp.parsePacket() >= GFNTP_PACKET_SIZE) {
			DEBUG_PRINTLN(F("\tGFNTP:reply received"));
			_udp.read(_pkt, GFNTP_PACKET_SIZE);
			// Parse NTP reply
			_currentTime = (unsigned long) _pkt[40] << 24;
			_currentTime |= (unsigned long) _pkt[41] << 16;
			_currentTime |= (unsigned long) _pkt[42] << 8;
			_currentTime |= (unsigned long) _pkt[43];
			_currentTime = _currentTime - 2208988800UL;
			// Print out received timestamp
			DEBUG_PRINT(F("\tGFNTP:received time "));
			_currentTime = _currentTime + _timeZone * 3600ul;
			_timeHelper = millis();
			DEBUG_PRINTLN(_currentTime);
			// Run callback function if set
			if (_onTimeReceived != 0) {
				DEBUG_PRINTLN(F("\tGFNTP:running NTP callback"));
				_onTimeReceived(_currentTime);
			}
			_errorUpdateInterval = 0;
			_gfntpstate = E_NTP_IDLE;
		} else if (millis() - _timeout > GFNTP_TIMEOUT) {
			// timeout if response took more than expected
			DEBUG_PRINTLN(F("\tGFNTP:timeout"));
			_gfntpstate = (_retries > GFNTP_RETRIES) ? E_NTP_IDLE : E_NTP_SEND_REQUEST;
		}

		// Release UDP resouces
		if (_gfntpstate == E_NTP_IDLE) {

			DEBUG_PRINTLN(F("\tGFNTP:end udp"));
			_udp.stop();
		}
		break;
	}
}

void GFNTP::sendNTPRequest()
{
	DEBUG_PRINTLN(F("\tGFNTP::sendNTPRequest()"));

	// Initialize values needed to form NTP request
	DEBUG_PRINT(F("\tGFNTP:sending..."));
	memset(_pkt, 0, GFNTP_PACKET_SIZE);
	_pkt[0] = 0b11100011;
	_pkt[1] = 0x00;
	_pkt[2] = 0x06;
	_pkt[3] = 0xEC;
	_pkt[12] = 0x49;
	_pkt[13] = 0x4E;
	_pkt[14] = 0x49;
	_pkt[15] = 0x52;
	// send out the request
	if (_udp.beginPacket(_serverName, 123)) {
		DEBUG_PRINT(F("BEGIN "));
		_udp.write(_pkt, GFNTP_PACKET_SIZE);
		DEBUG_PRINT(F("WRITE "));
	}
	if (_udp.endPacket()) {
		DEBUG_PRINT(F("END "));
	}
	DEBUG_PRINTLN();
}
