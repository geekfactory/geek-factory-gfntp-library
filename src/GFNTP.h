/* Geek Factory GFNTP Library for Arduino
 * 
 * Copyright (C) 2018 Jesus Ruben Santa Anna Zamudio.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Author website: https://www.geekfactory.mx
 * Author e-mail: ruben at geekfactory dot mx
 */

#ifndef GFNTP_h
#define GFNTP_h

/*-------------------------------------------------------------*
 *		Includes and dependencies			*
 *-------------------------------------------------------------*/
#include <Arduino.h>
#include <UDP.h>

/*-------------------------------------------------------------*
 *		Library configuration				*
 *-------------------------------------------------------------*/
/**
 * Uncomment this to enable debug messages
 */
#define CONFIG_GFNTP_DEBUG

/**
 * Configures the stream object used to send debug messages
 */
#define CONFIG_GFNTP_DEBUGSTREAM	Serial

/*-------------------------------------------------------------*
 *		Macros and definitions				*
 *-------------------------------------------------------------*/
#define GFNTP_VERSION_STRING	"1.0.0"
#define GFNTP_PACKET_SIZE		48
#define GFNTP_LOCAL_PORT		1337
#define GFNTP_RETRIES			10
#define GFNTP_TIMEOUT			5000
#define GFNTP_DEFAULT_SERVER	"pool.ntp.org"
#define GFNTP_DEFAULT_TIMEZONE	0
#define GFNTP_DEFAULT_UPDATEINTERVAL	0
#define GFNTP_ERROR_UPDATEINTERVAL	60

/*-------------------------------------------------------------*
 *		Typedefs enums & structs			*
 *-------------------------------------------------------------*/
/*
 * Type definition for a function that sends multiple chars to the remote
 * terminal (function pointer)
 */
typedef void (* gfntpcb_t)(uint32_t);

enum gfntp_states {
	/**
	 * FSM is waiting for next update cycle or user update command
	 */
	E_NTP_IDLE,
	/**
	 * FSM is sending NTP packet to server
	 */
	E_NTP_SEND_REQUEST,
	/**
	 * Wait for server response
	 */
	E_NTP_WAIT_RESPONSE
};

/*-------------------------------------------------------------*
 *		Class declaration				*
 *-------------------------------------------------------------*/
class GFNTP {
public:
	/**
	 * Constructors for the GFNTP object. Prepares the object for use and configures
	 * the appropiate basic parameters.
	 * 
	 * @param udp Sets the UDP instance used for datagram interchange.
	 * @param serverName Sets the NTP server name or IP address (null terminated
	 * string).
	 */
	GFNTP(UDP & udp, const char * serverName);
	
	/**
	 * Constructors for the GFNTP object. Prepares the object for use and configures
	 * the appropiate basic parameters.
	 * 
	 * @param udp Sets the UDP instance used for datagram interchange.
	 * @param timeZone Sets the time zone in order to set local time from UTC.
	 * @param updateInterval Sets the interval betweeen queries to NTP server.
	 */
	GFNTP(UDP & udp, uint32_t timeZone = GFNTP_DEFAULT_TIMEZONE, uint32_t updateInterval = GFNTP_DEFAULT_UPDATEINTERVAL);

	/**
	 * Constructors for the GFNTP object. Prepares the object for use and configures
	 * the appropiate basic parameters.
	 * 
	 * @param udp Sets the UDP instance used for datagram interchange.
	 * @param serverName Sets the NTP server name or IP address (null terminated
	 * string).
	 * @param timeZone Sets the time zone in order to set local time from UTC.
	 * @param updateInterval Sets the interval betweeen queries to NTP server.
	 */
	GFNTP(UDP & udp, const char * serverName, uint32_t timeZone = GFNTP_DEFAULT_TIMEZONE, uint32_t updateInterval = GFNTP_DEFAULT_UPDATEINTERVAL);

	/**
	 * Prepares the GFNTP object for operation. This method should be called from
	 * the setup() function.
	 */
	void begin();

	/**
	 * Performs the periodic functions for NTP service to work, this method should
	 * be called from the loop() function.
	 */
	void process(bool netready = true);

	/**
	 * Closes the GFNTP object and releases the UDP object used for this service.
	 */
	void end();

	/**
	 * Configures the server name.
	 * 
	 * @param serverName Sets the NTP server name or IP address (null terminated
	 * string).
	 */
	void setServerName(const char * serverName);

	/**
	 * Configures the time zone. This is used to adjust the received UTC time to 
	 * local time.
	 * 
	 * @param timeZone Sets the time zone configuration.
	 */
	void setTimeZone(int16_t timeZone);

	/**
	 * Gets the configured time zone
	 * 
	 * @return The configured time zone for NTP time adjustment.
	 */
	uint16_t getTimeZone();

	/**
	 * Sets the update interval. This is the time interval between queries to
	 * NTP server.
	 * 
	 * @param updateInterval Sets the interval betweeen queries to NTP server.
	 */
	void setUpdateInterval(uint32_t updateInterval);

	/**
	 * Gets the configured update interval for NTP.
	 * 
	 * @return The number of milliseconds between NTP updates.
	 */
	uint32_t getUpdateInterval();

	/**
	 * Sets the callback function that is executed when the GFNTP library receives
	 * a new time update from the NTP server.
	 * 
	 * @param callback Pointer to function that runs when NTP update is received
	 */
	void setCallback(gfntpcb_t callback);

	/**
	 * Gets the current time in unix fotmat.
	 */
	uint32_t getTime();

	/**
	 * Forces the NTP service to update time. A request is sent to the server each
	 * time this funcion is called.
	 */
	void forceUpdate();


private:
	/**
	 * UDP object used to send / receive UDP datagrams
	 */
	UDP & _udp;

	/**
	 * Buffer used to prepare NTP request packet
	 */
	uint8_t _pkt[GFNTP_PACKET_SIZE];

	/**
	 * State of the finite state machine
	 */
	enum gfntp_states _gfntpstate = E_NTP_IDLE;

	/**
	 * Holds pointer to server name string
	 */
	const char * _serverName = GFNTP_DEFAULT_SERVER;

	/**
	 * Variable to hold the time zone
	 */
	int16_t _timeZone = GFNTP_DEFAULT_TIMEZONE;

	/**
	 * Pointer to function that is called when time is received from server
	 */
	gfntpcb_t _onTimeReceived = 0;

	/**
	 * Set update interval
	 */
	uint32_t _updateInterval = GFNTP_DEFAULT_UPDATEINTERVAL;

	/**
	 * Counter variable for NTP retries
	 */
	uint8_t _retries = 0;

	/**
	 * Variable to hold unix time
	 */
	uint32_t _currentTime = 0;

	/**
	 * Used to update the time variable constantly
	 */
	uint32_t _timeHelper = 0;

	/**
	 * Variable used for NTP timeout
	 */
	uint32_t _timeout;

	/**
	 * Variable used to schedule updates
	 */
	uint32_t _lastUpdate = 0;

	/**
	 * Update interval variable, it´s used when the library cannot contact NTP server
	 */
	uint32_t _errorUpdateInterval = 0;
	
	/**
	 * Forces NTP update when set
	 */
	bool _forceFlag = false;

	/**
	 * Method used to send NTP request to server
	 */
	void sendNTPRequest();
};
#endif
// End of Header file